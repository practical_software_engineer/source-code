/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Connecting;

import java.sql.*;

/*
The import java.sql.* statement references the classes and interfaces that provide
access to the data types used to manage different aspects of your interaction with the
database server. These are required for all JDBC programs
*/

public class Connect
{
  public static void main (String[] args)
  {
    Connection conn = null;
    //String url = "jdbc:mysql://localhost/cookbook";
    String url = "jdbc:mysql://127.0.0.1/cookbook";
    String userName = "cbuser";
    String password = "cbpass";

    try
    {//Connecting to the server is a two-step process:

      /*
      First, register the database driver with
      JDBC by calling Class.forName() . The Class.forName() method requires a driver
      name; for Connector/J, use com.mysql.jdbc.Driver .
      */
      Class.forName ("com.mysql.jdbc.Driver").newInstance ();
      /*
      -- Beware of Class.forName()! --
      The example program Connect.java registers the JDBC driver like this:
      Class.forName ("com.mysql.jdbc.Driver").newInstance ();
      You’re supposed to be able to register drivers without invoking newInstance() , like so:
      Class.forName ("com.mysql.jdbc.Driver");
      However, that call doesn’t work for some Java implementations, so be sure to use new
      Instance() , or you may find yourself enacting the Java motto, “write once, debug ev‐
      erywhere.”
      */




      /*
      Then call DriverManager.get
      Connection() to initiate the connection and obtain a Connection object that maintains
      information about the state of the connection.
      */
      conn = DriverManager.getConnection (url, userName, password);//The URL string has this format: jdbc:driver://host_name/db_name

      System.out.println ("Connected , fuck yah!");
    }
    catch (Exception e)
    {
      System.err.println ("Cannot connect to server ");
      System.exit (1);
    }
    if (conn != null)
    {
      try
      {
        conn.close ();
        System.out.println ("Disconnected , fuck yah!");
      }
      catch (Exception e) { /* ignore close errors */ }
    }


  }
}