/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametesr_fron_option_file;

import java.sql.*;
import java.util.*;   // need this for properties file support

/**
 *
 * @author nadav
 */
public class ReadPropsFile {
    public static void main (String[] args)
  {
    Connection conn = null;
    String url = null;
    String propsFile = "Cookbook.properties";
    Properties props = new Properties ();

    try
    {
      props.load (ReadPropsFile.class.getResourceAsStream (propsFile));
    }
    catch (Exception e)
    {
      System.err.println ("Cannot read properties file");
      System.exit (1);
    }
    try
    {
      // construct connection URL, encoding username
      // and password as parameters at the end
      url = "jdbc:mysql://"
            + props.getProperty ("host")
            + "/cookbook"
            + "?user=" + props.getProperty ("user")
            + "&password=" + props.getProperty ("password");
      Class.forName ("com.mysql.jdbc.Driver").newInstance ();
      conn = DriverManager.getConnection (url);
      System.out.println ("Connected");
      /*
      Statement s = conn.createStatement();
      s.executeQuery("select id,name,cats from profile");
      ResultSet rs = s.getResultSet();
      int count = 0;
      while(rs.next())
      {
          int id = rs.getInt(1);
          String name = rs.getString(2);
          int cats = rs.getInt(3);
          System.out.println(id+" , "+name+" , "+cats);
      }
      rs.close();
      s.close();
      
      */
      
    }
    catch (Exception e)
    {
      System.err.println ("Cannot connect to server");
    }
    finally
    {
      try
      {
        if (conn != null)
        {
          conn.close ();
          System.out.println ("Disconnected");
        }
      }
      catch (SQLException e) { /* ignore close errors */ }
    }
  }
}
