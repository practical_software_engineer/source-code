/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Writing_Library_Fiels;

import java.sql.*;

/**
 *
 * @author nadav
 */
public class Placeholder {
    
  public static void main (String[] args)
  {
    Connection conn = null;
      PreparedStatement s = null;
      ResultSet rs = null;
      
      try{
          
          conn = Cookbook.connect();
          s = conn.prepareStatement("INSERT INTO profile (name,birth,color,foods,cats) VALUES(?,?,?,?,?)");
          
          s.setString (1, "Amir");         // bind values to placeholders
          s.setString (2, "1997-01-12");
          s.setNull (3, java.sql.Types.CHAR);
          s.setString (4, "eggroll");
          s.setInt (5, 1);
          
          s.execute();
          rs = s.getResultSet();
          
          s.cancel();
          conn.close();
      }
      catch(Exception e)
      {
          Cookbook.printErrorMessage (e);
      }
      
  }
    
}
