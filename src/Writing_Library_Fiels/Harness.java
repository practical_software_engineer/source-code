/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Writing_Library_Fiels;


import java.sql.*;
/**
 *
 * @author nadav
 */
public class Harness
{
  public static void main (String[] args)
  {
    Connection conn = null;
    try
    {
        int x;
      conn = Cookbook.connect ();
      System.out.println ("Connected");
      //my additional code : -------------------------------------
      
      Statement s = conn.createStatement();
      s.executeQuery("select id,name,cats from profile");
      ResultSet rs = s.getResultSet();
      int count = 0;
      while(rs.next())
      {
          int id = rs.getInt(1);
          String name = rs.getString(2);
          int cats = rs.getInt(3);
          System.out.println(id+" , "+name+" , "+cats);
      }
      rs.close();
      s.close();
      
      //---------------------------------------
    }
    catch (Exception e)
    {
      Cookbook.printErrorMessage (e);
      System.exit (1);
    }
    finally
    {
      if (conn != null)
      {
        try
        {
          conn.close ();
          System.out.println ("Disconnected");
        }
        catch (Exception e)
        {
          String err = Cookbook.getErrorMessage (e);
          System.out.println (err);
        }
      }
    }
  }
}